package com.tlima.demoPersistencia;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;


public class LoginActivity extends ActionBarActivity {

    public static final String PREFERENCE = "DemoPersistencia";
    public static final String PREF_USUARIO = "usuario";
    public static final String PREF_SENHA = "senha";
    public static final String PREF_MANTER_CONECTADO = "manterConectado";
    private SharedPreferences configLogin;

    private EditText usuario;
    private EditText senha;
    private CheckBox manterConectado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        configLogin = getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);

        usuario = (EditText) findViewById(R.id.usuario);
        senha = (EditText) findViewById(R.id.senha);
        manterConectado = (CheckBox) findViewById(R.id.manter);

        usuario.setText(configLogin.getString(PREF_USUARIO, ""));
        senha.setText(configLogin.getString(PREF_SENHA, ""));

        if (configLogin.getBoolean(PREF_MANTER_CONECTADO, false)) {
            if (isUsuarioValido(usuario.getText().toString(), senha.getText().toString())) {
                abrirTela();
            }
        }
    }

    public void conectar(View view) {
        if (isUsuarioValido(usuario.getText().toString(), senha.getText().toString())) {
            if (manterConectado.isChecked()) {
                manterLogado();
            }
            abrirTela();
        } else {
            Toast.makeText(this, getString(R.string.usuario_invalido), Toast.LENGTH_SHORT).show();
        }
    }

    private void manterLogado() {
        SharedPreferences.Editor editor = configLogin.edit();

        editor.putString(PREF_USUARIO, usuario.getText().toString());
        editor.putString(PREF_SENHA, senha.getText().toString());
        editor.putBoolean(PREF_MANTER_CONECTADO, manterConectado.isChecked());

        editor.commit();
    }

    private boolean isUsuarioValido(String usuario, String senha) {
        return "usuario".equals(usuario) && "senha".equals(senha);
    }

    private void abrirTela() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }
}
