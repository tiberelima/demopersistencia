package com.tlima.demoPersistencia;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tlima.demoPersistencia.dao.PessoaDAO;
import com.tlima.demoPersistencia.model.Pessoa;


public class MainActivity extends ActionBarActivity {

    private SharedPreferences configLogin;
    private EditText nome;
    private EditText idade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        configLogin = getSharedPreferences(LoginActivity.PREFERENCE, Context.MODE_PRIVATE);

        TextView usuario = (TextView) findViewById(R.id.usuario);
        usuario.setText(configLogin.getString(LoginActivity.PREF_USUARIO, ""));

        nome = (EditText) findViewById(R.id.nome);
        idade = (EditText) findViewById(R.id.idade);
    }

    public void desconectar(View view) {
        SharedPreferences.Editor editor = configLogin.edit();
        editor.clear();
        editor.commit();
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    public void salvar(View view) {
        try {
            if (nome.getText().toString().isEmpty() || idade.getText().toString().isEmpty()) {
                Toast.makeText(this, R.string.preencher_form, Toast.LENGTH_SHORT).show();
            } else {
                Pessoa pessoa = new Pessoa();
                pessoa.setNome(nome.getText().toString());
                pessoa.setIdade(Integer.valueOf(idade.getText().toString()));

                PessoaDAO dao = new PessoaDAO(this);
                dao.salvar(pessoa);

                Toast.makeText(this, R.string.gravado_com_sucesso, Toast.LENGTH_SHORT).show();

                limparFormulario();
            }
        } catch (NumberFormatException e) {
            Toast.makeText(this, R.string.valor_idade_invalido, Toast.LENGTH_SHORT).show();
        }
    }

    private void limparFormulario() {
        nome.setText("");
        idade.setText("");
    }

    public void cancelar(View view) {
        limparFormulario();
    }

    public void listar(View view) {
        startActivity(new Intent(this, ListaPessoaActivity.class));
    }
}
