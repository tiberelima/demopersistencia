package com.tlima.demoPersistencia;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.ListView;

import com.tlima.demoPersistencia.adapter.ListaPessoaAdapter;
import com.tlima.demoPersistencia.dao.PessoaDAO;
import com.tlima.demoPersistencia.model.Pessoa;

import java.util.List;

public class ListaPessoaActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_pessoa);

        PessoaDAO dao = new PessoaDAO(this);
        List<Pessoa> pessoas = dao.listarTodos();

        ListView listaPessoa = (ListView) findViewById(R.id.lista_pessoa);
        listaPessoa.setAdapter(new ListaPessoaAdapter(this, pessoas));
    }
}
