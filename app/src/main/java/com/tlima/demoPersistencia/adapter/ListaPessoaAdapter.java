package com.tlima.demoPersistencia.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tlima.demoPersistencia.R;
import com.tlima.demoPersistencia.model.Pessoa;

import java.util.List;

public class ListaPessoaAdapter extends BaseAdapter {

    private final Context context;
    private final List<Pessoa> pessoas;

    public ListaPessoaAdapter(Context context, List<Pessoa> pessoas) {
        this.context = context;
        this.pessoas = pessoas;
    }

    @Override
    public int getCount() {
        return pessoas.size();
    }

    @Override
    public Object getItem(int position) {
        return pessoas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return pessoas.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_pessoa_layout, parent, false);
        }

        Pessoa pessoa = pessoas.get(position);

        TextView nome = (TextView) convertView.findViewById(R.id.nome);
        TextView idade = (TextView) convertView.findViewById(R.id.idade);

        nome.setText(pessoa.getNome());
        idade.setText(String.valueOf(pessoa.getIdade()));
        
        return convertView;
    }
}
