package com.tlima.demoPersistencia.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.tlima.demoPersistencia.model.Pessoa;

import java.util.ArrayList;
import java.util.List;

public class PessoaDAO {

    private DBHelper dbHelper;
    private Context context;

    public PessoaDAO(Context context) {
        this.context = context;
        dbHelper = new DBHelper(context);
    }

    public void salvar(@NonNull Pessoa pessoa) {
        if (pessoa.getId() == null) {
            inserir(pessoa);
        } else {
            atualizar(pessoa);
        }
    }

    private void inserir(Pessoa pessoa) {
        ContentValues values = new ContentValues();
        values.put("nome", pessoa.getNome());
        values.put("idade", pessoa.getIdade());

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long id = db.insert("pessoa", null, values);
        pessoa.setId(id);
        db.close();
    }

    private void atualizar(Pessoa pessoa) {
        ContentValues values = new ContentValues();
        values.put("nome", pessoa.getNome());
        values.put("idade", pessoa.getId());

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.update("pessoa", values, "_id = ?", new String[]{pessoa.getId().toString()});
        db.close();
    }

    public List<Pessoa> listarTodos() {
        List<Pessoa> pessoas = new ArrayList<>();

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query("pessoa", null, null, null, null, null, "nome asc");

        try {
            while (cursor.moveToNext()) {
                Pessoa pessoa = new Pessoa();
                pessoa.setId(cursor.getLong(cursor.getColumnIndex("_id")));
                pessoa.setIdade(cursor.getInt(cursor.getColumnIndex("idade")));
                pessoa.setNome(cursor.getString(cursor.getColumnIndex("nome")));

                pessoas.add(pessoa);
            }
        } catch (Exception e) {
        } finally {
            cursor.close();
        }
        db.close();

        return pessoas;
    }
}
